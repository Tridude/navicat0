package com.archfred.navicat0;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public class KmlGeometry {
	private LatLng point;
	private ArrayList<LatLng> polygon;
	
	public KmlGeometry(LatLng point) {
		this.point = point;
		this.polygon = new ArrayList<LatLng>();
	}
	
	public KmlGeometry(double latitude, double longitude) {
		this.point = new LatLng(latitude, longitude);
		this.polygon = new ArrayList<LatLng>();
	}
	
	public KmlGeometry(LatLng point, ArrayList<LatLng> polygon) {
		this.point = point;
		this.polygon = polygon;
	}
	
	public LatLng getPoint() {
		return point;
	}
	
	public void setPoint(double latitude, double longitude) {
		this.point = new LatLng(latitude, longitude);
	}
	
	public void setPoint(LatLng point) {
		this.point = point;
	}
	
	public void addPolyLatLng(double latitude, double longitude) {
		this.polygon.add(new LatLng(latitude, longitude));
	}
	
	public ArrayList<LatLng> getPolygon() {
		return this.polygon;
	}
	
	
	
}
