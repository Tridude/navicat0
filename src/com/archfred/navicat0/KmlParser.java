package com.archfred.navicat0;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.*;

import com.google.android.gms.maps.model.LatLng;

import android.util.Log;
import android.util.Xml;

public class KmlParser {
	
	private static final String ns = null;
	
	public List initializeParser(InputStream is) throws XmlPullParserException, IOException {
		try{
			XmlPullParser xpp = Xml.newPullParser();
			xpp.setInput(is, null);
			xpp.nextTag();
			xpp.nextTag();
			return parse(xpp);
		} finally {
			is.close();
		}
	}
	
	public List parse(XmlPullParser xpp) throws XmlPullParserException, IOException {
		ArrayList<KmlPlacemark> placemarks = new ArrayList();
		
		xpp.require(XmlPullParser.START_TAG, ns, "Document");
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("Placemark")) {
				placemarks.add(parsePlacemark(xpp));
			} else {
				skip(xpp);
			}
		}
		return placemarks;
	}
		
	private KmlPlacemark parsePlacemark(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String name = "";
		String description = "";
		KmlGeometry geometry = null;
		
		xpp.require(XmlPullParser.START_TAG, ns, "Placemark");
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("name")) {
				name = parseName(xpp);
			} else if (tag.equals("description")) {
				description = parseDescription(xpp);
			} else if (tag.equals("MultiGeometry")) {
				geometry = parseGeometry(xpp);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "Placemark");
		return new KmlPlacemark(name, description, geometry);
	}
	
	private String parseName(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "name");
		String name = parseText(xpp);
		xpp.require(XmlPullParser.END_TAG, ns, "name");
		return name;
	}
	
	private String parseDescription(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "description");
		String description = parseText(xpp);
		xpp.require(XmlPullParser.END_TAG, ns, "description");
		return description;
	}
	
	private KmlGeometry parseGeometry(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "MultiGeometry");
		LatLng point = null;
		ArrayList<LatLng> polygon = null;
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("Point")) {
				point = parsePoint(xpp);
			} else if (tag.equals("Polygon")) {
				polygon = parsePolygon(xpp);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "MultiGeometry");
		return new KmlGeometry(point, polygon);
	}
	
	private LatLng parsePoint(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "Point");
		LatLng point = null;
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("coordinates")) {
				point = parseCoordinates(xpp).get(0);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "Point");
		return point;
	}
	
	private ArrayList<LatLng> parsePolygon(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "Polygon");
		ArrayList<LatLng> polygon = null;
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("outerBoundaryIs")) {
				polygon = parseOuterBoundaryIs(xpp);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "Polygon");
		return polygon;
	}
	
	private ArrayList<LatLng> parseOuterBoundaryIs(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "outerBoundaryIs");
		ArrayList<LatLng> polygon = null;
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("LinearRing")) {
				polygon = parseLinearRing(xpp);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "outerBoundaryIs");
		return polygon;
	}
	
	private ArrayList<LatLng> parseLinearRing(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "LinearRing");
		ArrayList<LatLng> polygon = null;
		while (xpp.next() != XmlPullParser.END_TAG) {
			if (xpp.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String tag = xpp.getName();
			if (tag.equals("coordinates")) {
				polygon = parseCoordinates(xpp);
			} else {
				skip(xpp);
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "LinearRing");
		return polygon;
	}
	
	private ArrayList<LatLng> parseCoordinates(XmlPullParser xpp) throws XmlPullParserException, IOException {
		xpp.require(XmlPullParser.START_TAG, ns, "coordinates");
		ArrayList<LatLng> coordinates = new ArrayList<LatLng>();
		String strCoordinates[] = parseText(xpp).split(" ");
		for (int i=0; i<strCoordinates.length; i++) {
			String strCoordinate[] = strCoordinates[i].split(",");
			if (strCoordinate.length >= 2) {
				coordinates.add(new LatLng(Double.parseDouble(strCoordinate[1]), Double.parseDouble(strCoordinate[0])));
			}
		}
		xpp.require(XmlPullParser.END_TAG, ns, "coordinates");
		return coordinates;
	}
	
	private String parseText(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String text = "";
		if (xpp.next() == XmlPullParser.TEXT){
			text = xpp.getText();
			xpp.nextTag();
		}
		return text;
	}
	
	private void skip(XmlPullParser xpp) throws XmlPullParserException, IOException {
		if (xpp.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (xpp.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}
	
	
}
