package com.archfred.navicat0;

import java.util.ArrayList;

public interface AsyncResponse {
	void processFinish(ArrayList<KmlPlacemark> result);
}