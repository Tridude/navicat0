package com.archfred.navicat0;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

public class KmlPlacemark {

	private String name;
	private String description;
	private KmlGeometry geometry;
	private String id = null;
	
	public KmlPlacemark(String name, String description, KmlGeometry Geometry) {
		this.setName(name);
		this.setDescription(description);
		this.setGeometry(Geometry);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public KmlGeometry getGeometry() {
		return geometry;
	}

	public void setGeometry(KmlGeometry geometry) {
		this.geometry = geometry;
	}
	
	public LatLng getPoint() {
		return this.geometry.getPoint();
	}
	
	public ArrayList<LatLng> getPolygon() {
		return this.geometry.getPolygon();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public boolean compareId(String id) {
		return this.id.equals(id); 
	}

}
