package com.archfred.navicat0;

import java.net.URL;
import java.util.ArrayList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

public class ShowMapActivity extends FragmentActivity implements AsyncResponse {

	public final static String EXTRA_MESSAGE = "com.archfred.vaicat0.MESSAGE";
	
	private ShowMapActivity showMap = this;
	private GoogleMap mMap = null;
	private UiSettings mUi = null;
	private LatLngBounds CSUCI = new LatLngBounds( new LatLng(34.159,-119.036), new LatLng(34.165,-119.05));
	
	private ArrayList<KmlPlacemark> placemarks = new ArrayList<KmlPlacemark>();
	
	private LongOperation kmlFetch = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_map);
        if (savedInstanceState == null) {
        	// Need to fix. onCreate is being ran when up button is used from ShowPlaceActivity
        	Log.d("mapkml", "Starting...");
        	runBackTask("");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.show_map, menu);
        return true;
    }
    
//    @Override
//    protected void onSaveInstanceState(Bundle savedInstanceState) {
//    	super.onSaveInstanceState(savedInstanceState);
//    	savedInstanceState.putParcelableArrayList("placemarks", placemarks);
//    }
//    
    private int getIndexOfPlacemark(String id) {
    	for (int i=0; i<this.placemarks.size(); i++) {
    		//Log.d("mapkml", "Comparing %s to %s".format(id, placemarks.get(i).getId()));
    		if (placemarks.get(i).compareId(id)) {
    			return i;
    		}
    	}
    	return -1;
    }
    
    private void setUpMapIfNeeded() {
    	if (mMap == null) {
    		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
    		
    		if (mMap != null) {
    			//mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(CSUCI, 0));
    			mMap.setMyLocationEnabled(true);
    			mUi = mMap.getUiSettings();
    			mUi.setCompassEnabled(true);
    			mUi.setMyLocationButtonEnabled(true);
    			mUi.setTiltGesturesEnabled(false);
    			
    			mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
					
					@Override
					public void onInfoWindowClick(Marker marker) {
						// TODO Auto-generated method stub
						//Log.d("mapkml", marker.getTitle());
						//Log.d("mapkml", marker.getId());
						int placemarkIndex = getIndexOfPlacemark(marker.getId());
						if (placemarkIndex < 0) {
							Log.d("mapkml", "cannot find placemark by id");
							return;
						}
						KmlPlacemark placemark = placemarks.get(placemarkIndex);
						//Log.d("mapkml", placemark.getDescription());
						Intent intent = new Intent(showMap, ShowPlaceActivity.class);
						intent.putExtra(EXTRA_MESSAGE, placemark.getDescription());
						startActivity(intent);
					}
				});
    			//Log.d("mapkml", "good");
    		} else {
    			//Log.d("mapkml", "bad");
    		}
    	}
    }
    
    private class LongOperation extends AsyncTask<ArrayList<KmlPlacemark>, Void, ArrayList<KmlPlacemark>> {
    	public AsyncResponse delegate = null;
    	
    	@Override
    	protected ArrayList<KmlPlacemark> doInBackground(ArrayList<KmlPlacemark>... placemarks) {
    		ArrayList<KmlPlacemark> result = new ArrayList<KmlPlacemark>();
    		try {
    			URL url = new URL("http://student.csuci.edu/~fred.contrata639/public-pmos.kml");
    			KmlParser kml = new KmlParser();
    			result = (ArrayList<KmlPlacemark>) kml.initializeParser(url.openStream());
    		} catch (Exception e) {
    			System.out.println("err: " + e);
    		}
    		return result;
    	}
    	
    	@Override
    	protected void onPostExecute(ArrayList<KmlPlacemark> result) {
    		for (KmlPlacemark placemark : result) {
				//Log.d("place", "name: " + placemark.getName());
				//Log.d("place", "desc: " + placemark.getDescription());
				//Log.d("place", "loc: " + placemark.printCoordinates());
				Marker currentMarker = mMap.addMarker(new MarkerOptions()
				.position(placemark.getPoint())
				.title(placemark.getName()));
				placemark.setId(currentMarker.getId());
				mMap.addPolygon(new PolygonOptions().addAll(placemark.getPolygon()).strokeWidth(5));
			}
    		delegate.processFinish(result);
    	}
    }
    
    private void runBackTask(String url) {
    	setUpMapIfNeeded();
    	kmlFetch = new LongOperation();
    	kmlFetch.delegate = this;
    	kmlFetch.execute(placemarks);
    }

	@Override
	public void processFinish(ArrayList<KmlPlacemark> result) {
		this.placemarks = result;		
	}
    
}
